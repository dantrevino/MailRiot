import Vue from "vue";
import Vuex from "vuex";

const faker = require("faker");

Vue.use(Vuex);

class Email {
  constructor() {
    this.id = 1; // uuidv4
    this.from = faker.name.findName();
    (this.timestamp = null),
      (this.subject = faker.lorem.sentence().substring(0, 20)),
      (this.snippet = faker.lorem.lines(1)),
      (this.fullMail = faker.lorem.paragraphs(faker.random.number(40))),
      (this.email = faker.internet.email());
  }
}

export default new Vuex.Store({
  state: {
    emails: []
  },
  mutations: {
    ADD_EMAIL(state, email) {
      state.emails.unshift(email);
    }
  },
  actions: {
    addEmail(context, email) {
      context.commit("ADD_EMAIL", email);
    },
    init(context) {
      for (let j = 0; j < 23; j++) {
        let email = new Email();
        email.id = j;
        context.commit("ADD_EMAIL", email);
      }
    }
  },
  getters: {
    emails: state => state.emails
  }
});
